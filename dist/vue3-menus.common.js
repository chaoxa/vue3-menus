module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "21a1":
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__("24fb");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".menus-item{display:flex;line-height:2rem;padding:0 1rem;margin:0;font-size:.8rem;outline:0;align-items:center;transition:.2s;box-sizing:border-box;list-style:none;border-bottom:1px solid transparent}.menus-item-divided{border-bottom-color:#ebeef5}.menus-item .menus-item-icon{display:flex;margin-right:.6rem;width:1rem}.menus-item .menus-item-label{flex:1;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.menus-item .menus-item-suffix{margin-left:1.5rem;font-size:.39rem;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.menus-item-available{color:#606266;cursor:pointer}.menus-item-available:hover{background:#ecf5ff;color:#409eff}.menus-item-disabled{color:#c0c4cc;cursor:not-allowed}.menus-item-active{background:#ecf5ff;color:#409eff}.menus-item-tip{font-size:9px;color:#999}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "24fb":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "2789":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("4187");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("4c637b10", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "4187":
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__("24fb");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".menus[data-v-347764fc]{position:fixed;box-shadow:0 2px 4px rgba(0,0,0,.12),0 0 6px rgba(0,0,0,.04);background:#fff;border-radius:4px;padding:8px 0;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;box-sizing:border-box}.menus_body[data-v-347764fc]{display:block}.menus-fade-enter-active[data-v-347764fc],.menus-fade-leave-active[data-v-347764fc]{transition:opacity .1s ease-in-out}.menus-fade-enter-from[data-v-347764fc],.menus-fade-leave-to[data-v-347764fc]{opacity:0}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "499e":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "default", function() { return /* binding */ addStylesClient; });

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/listToStyles.js
/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/addStylesClient.js
/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/



var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

function addStylesClient (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "8875":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// addapted from the document.currentScript polyfill by Adam Miller
// MIT license
// source: https://github.com/amiller-gh/currentScript-polyfill

// added support for Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1620505

(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
}(typeof self !== 'undefined' ? self : this, function () {
  function getCurrentScript () {
    var descriptor = Object.getOwnPropertyDescriptor(document, 'currentScript')
    // for chrome
    if (!descriptor && 'currentScript' in document && document.currentScript) {
      return document.currentScript
    }

    // for other browsers with native support for currentScript
    if (descriptor && descriptor.get !== getCurrentScript && document.currentScript) {
      return document.currentScript
    }
  
    // IE 8-10 support script readyState
    // IE 11+ & Firefox support stack trace
    try {
      throw new Error();
    }
    catch (err) {
      // Find the second match for the "at" string to get file src url from stack.
      var ieStackRegExp = /.*at [^(]*\((.*):(.+):(.+)\)$/ig,
        ffStackRegExp = /@([^@]*):(\d+):(\d+)\s*$/ig,
        stackDetails = ieStackRegExp.exec(err.stack) || ffStackRegExp.exec(err.stack),
        scriptLocation = (stackDetails && stackDetails[1]) || false,
        line = (stackDetails && stackDetails[2]) || false,
        currentLocation = document.location.href.replace(document.location.hash, ''),
        pageSource,
        inlineScriptSourceRegExp,
        inlineScriptSource,
        scripts = document.getElementsByTagName('script'); // Live NodeList collection
  
      if (scriptLocation === currentLocation) {
        pageSource = document.documentElement.outerHTML;
        inlineScriptSourceRegExp = new RegExp('(?:[^\\n]+?\\n){0,' + (line - 2) + '}[^<]*<script>([\\d\\D]*?)<\\/script>[\\d\\D]*', 'i');
        inlineScriptSource = pageSource.replace(inlineScriptSourceRegExp, '$1').trim();
      }
  
      for (var i = 0; i < scripts.length; i++) {
        // If ready state is interactive, return the script tag
        if (scripts[i].readyState === 'interactive') {
          return scripts[i];
        }
  
        // If src matches, return the script tag
        if (scripts[i].src === scriptLocation) {
          return scripts[i];
        }
  
        // If inline source matches, return the script tag
        if (
          scriptLocation === currentLocation &&
          scripts[i].innerHTML &&
          scripts[i].innerHTML.trim() === inlineScriptSource
        ) {
          return scripts[i];
        }
      }
  
      // If no match, return null
      return null;
    }
  };

  return getCurrentScript
}));


/***/ }),

/***/ "8bbf":
/***/ (function(module, exports) {

module.exports = require("vue");

/***/ }),

/***/ "92c7":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_v16_dist_index_js_ref_0_1_MenusItem_vue_vue_type_style_index_0_id_11a672fa_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("d1aa");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_v16_dist_index_js_ref_0_1_MenusItem_vue_vue_type_style_index_0_id_11a672fa_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_v16_dist_index_js_ref_0_1_MenusItem_vue_vue_type_style_index_0_id_11a672fa_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "bbcf":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_v16_dist_index_js_ref_0_1_Menus_vue_vue_type_style_index_0_id_347764fc_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("2789");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_v16_dist_index_js_ref_0_1_Menus_vue_vue_type_style_index_0_id_347764fc_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_6_oneOf_1_1_node_modules_vue_loader_v16_dist_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_2_node_modules_postcss_loader_src_index_js_ref_6_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_v16_dist_index_js_ref_0_1_Menus_vue_vue_type_style_index_0_id_347764fc_scoped_true_lang_css__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "d1aa":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("21a1");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("10b4684a", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "Vue3Menus", function() { return /* reexport */ Vue3Menus; });
__webpack_require__.d(__webpack_exports__, "directive", function() { return /* reexport */ directive; });
__webpack_require__.d(__webpack_exports__, "menusEvent", function() { return /* reexport */ menusEvent; });

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (true) {
    var getCurrentScript = __webpack_require__("8875")
    currentScript = getCurrentScript()

    // for backward compatibility, because previously we directly included the polyfill
    if (!('currentScript' in document)) {
      Object.defineProperty(document, 'currentScript', { get: getCurrentScript })
    }
  }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// EXTERNAL MODULE: external {"commonjs":"vue","commonjs2":"vue","root":"Vue"}
var external_commonjs_vue_commonjs2_vue_root_Vue_ = __webpack_require__("8bbf");

// CONCATENATED MODULE: ./node_modules/vue-loader-v16/dist/templateLoader.js??ref--5!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader-v16/dist??ref--0-1!./package/components/Menus.vue?vue&type=template&id=347764fc&scoped=true

const _withId = /*#__PURE__*/Object(external_commonjs_vue_commonjs2_vue_root_Vue_["withScopeId"])("data-v-347764fc")

Object(external_commonjs_vue_commonjs2_vue_root_Vue_["pushScopeId"])("data-v-347764fc")
const _hoisted_1 = { class: "menus_body" }
Object(external_commonjs_vue_commonjs2_vue_root_Vue_["popScopeId"])()

const Menusvue_type_template_id_347764fc_scoped_true_render = /*#__PURE__*/_withId(function render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_MenusItem = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["resolveComponent"])("MenusItem")

  return (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])(external_commonjs_vue_commonjs2_vue_root_Vue_["Teleport"], { to: "body" }, [
    Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createVNode"])(external_commonjs_vue_commonjs2_vue_root_Vue_["Transition"], { name: "menus-fade" }, {
      default: _withId(() => [
        (_ctx.open)
          ? (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])("div", {
              key: 0,
              ref: "menusRef",
              class: "menus",
              style: { ..._ctx.menusStyle, top: `${_ctx.style.top}px`, left: `${_ctx.style.left}px`, minWidth: _ctx.style.minWidth, maxWidth: _ctx.style.maxWidth, zIndex: _ctx.style.zIndex },
              onContextmenu: _cache[1] || (_cache[1] = (e) => e.preventDefault()),
              onMousewheel: _cache[2] || (_cache[2] = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["withModifiers"])(() => {}, ["stop"]))
            }, [
              Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createVNode"])("div", _hoisted_1, [
                (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(true), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])(external_commonjs_vue_commonjs2_vue_root_Vue_["Fragment"], null, Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderList"])(_ctx.menus, (item, index) => {
                  return (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])(external_commonjs_vue_commonjs2_vue_root_Vue_["Fragment"], { key: index }, [
                    (!item.hidden)
                      ? (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])(_component_MenusItem, {
                          key: 0,
                          item: item,
                          index: index,
                          activeIndex: _ctx.activeIndex,
                          onMenusEnter: _ctx.menusEnter,
                          menusItemClass: _ctx.menusItemClass,
                          hasIcon: _ctx.hasIcon
                        }, Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createSlots"])({ _: 2 }, [
                          (_ctx.slots.default)
                            ? {
                                name: "default",
                                fn: _withId(({ item }) => [
                                  Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "default", { item: item })
                                ])
                              }
                            : undefined,
                          (!_ctx.slots.default && _ctx.slots.icon)
                            ? {
                                name: "icon",
                                fn: _withId(({ item }) => [
                                  Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "icon", { item: item })
                                ])
                              }
                            : undefined,
                          (!_ctx.slots.default && _ctx.slots.label)
                            ? {
                                name: "label",
                                fn: _withId(({ item }) => [
                                  Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "label", { item: item })
                                ])
                              }
                            : undefined,
                          (!_ctx.slots.default && _ctx.slots.suffix)
                            ? {
                                name: "suffix",
                                fn: _withId(({ item }) => [
                                  Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "suffix", { item: item })
                                ])
                              }
                            : undefined
                        ]), 1032, ["item", "index", "activeIndex", "onMenusEnter", "menusItemClass", "hasIcon"]))
                      : Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createCommentVNode"])("", true)
                  ], 64 /* STABLE_FRAGMENT */))
                }), 128 /* KEYED_FRAGMENT */))
              ])
            ], 36))
          : Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createCommentVNode"])("", true)
      ]),
      _: 1
    })
  ]))
})
// CONCATENATED MODULE: ./package/components/Menus.vue?vue&type=template&id=347764fc&scoped=true

// CONCATENATED MODULE: ./node_modules/vue-loader-v16/dist/templateLoader.js??ref--5!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader-v16/dist??ref--0-1!./package/components/MenusItem.vue?vue&type=template&id=11a672fa


const MenusItemvue_type_template_id_11a672fa_hoisted_1 = {
  key: 0,
  class: "menus-item-icon"
}
const _hoisted_2 = { class: "menus-item-label" }
const _hoisted_3 = { class: "menus-item-suffix" }
const _hoisted_4 = /*#__PURE__*/Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createTextVNode"])("▶")
const _hoisted_5 = {
  key: 3,
  class: "menus-item-tip"
}

function MenusItemvue_type_template_id_11a672fa_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_MenusIcon = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["resolveComponent"])("MenusIcon")

  return (_ctx.slots.default)
    ? (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])("div", {
        key: 0,
        onMouseenter: _cache[1] || (_cache[1] = ($event) => _ctx.menusEnter($event, _ctx.item)),
        onClick: _cache[2] || (_cache[2] = ($event) => _ctx.menusClick($event, _ctx.item)),
        onContextmenu: _cache[3] || (_cache[3] = ($event) => _ctx.menusClick($event, _ctx.item))
      }, [
        Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "default", {
          item: { activeIndex: _ctx.activeIndex, item: _ctx.item }
        })
      ], 32))
    : (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])("div", {
        key: 1,
        onMouseenter: _cache[4] || (_cache[4] = ($event) => _ctx.menusEnter($event, _ctx.item)),
        onClick: _cache[5] || (_cache[5] = ($event) => _ctx.menusClick($event, _ctx.item)),
        onContextmenu: _cache[6] || (_cache[6] = ($event) => _ctx.menusClick($event, _ctx.item)),
        style: _ctx.item.style ? _ctx.item.style : {},
        class: ['menus-item', _ctx.item.disabled ? 'menus-item-disabled' : 'menus-item-available',
      _ctx.item.divided ? 'menus-divided' : null, (!_ctx.item.disabled && _ctx.activeIndex === _ctx.index) ? 'menus-item-active' : null,
      _ctx.menusItemClass]
      }, [
        (_ctx.hasIcon)
          ? (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])("div", MenusItemvue_type_template_id_11a672fa_hoisted_1, [
              (_ctx.slots.icon)
                ? Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "icon", {
                    key: 0,
                    item: { activeIndex: _ctx.activeIndex, item: _ctx.item }
                  })
                : (_ctx.item.icon)
                  ? (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])(external_commonjs_vue_commonjs2_vue_root_Vue_["Fragment"], { key: 1 }, [
                      (typeof _ctx.item.icon === 'string')
                        ? (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])("span", {
                            key: 0,
                            innerHTML: _ctx.item.icon
                          }, null, 8, ["innerHTML"]))
                        : (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])(_component_MenusIcon, {
                            key: 1,
                            options: _ctx.item.icon
                          }, null, 8, ["options"]))
                    ], 64 /* STABLE_FRAGMENT */))
                  : Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createCommentVNode"])("", true)
            ]))
          : Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createCommentVNode"])("", true),
        Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createVNode"])("span", _hoisted_2, [
          (_ctx.slots.label)
            ? Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "label", {
                key: 0,
                item: { activeIndex: _ctx.activeIndex, item: _ctx.item }
              })
            : (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])(external_commonjs_vue_commonjs2_vue_root_Vue_["Fragment"], { key: 1 }, [
                Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createTextVNode"])(Object(external_commonjs_vue_commonjs2_vue_root_Vue_["toDisplayString"])(_ctx.item.label), 1 /* TEXT */)
              ], 64 /* STABLE_FRAGMENT */))
        ]),
        Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createVNode"])("div", _hoisted_3, [
          (_ctx.item.children && _ctx.slots.suffix)
            ? Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "suffix", {
                key: 0,
                item: { activeIndex: _ctx.activeIndex, item: _ctx.item }
              })
            : (_ctx.item.children)
              ? (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])(external_commonjs_vue_commonjs2_vue_root_Vue_["Fragment"], { key: 1 }, [
                  _hoisted_4
                ], 64 /* STABLE_FRAGMENT */))
              : (_ctx.item.tip && _ctx.slots.suffix)
                ? Object(external_commonjs_vue_commonjs2_vue_root_Vue_["renderSlot"])(_ctx.$slots, "suffix", {
                    key: 2,
                    item: { activeIndex: _ctx.activeIndex, item: _ctx.item }
                  })
                : (_ctx.item.tip)
                  ? (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["openBlock"])(), Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createBlock"])("span", _hoisted_5, Object(external_commonjs_vue_commonjs2_vue_root_Vue_["toDisplayString"])(_ctx.item.tip), 1))
                  : Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createCommentVNode"])("", true)
        ])
      ], 38))
}
// CONCATENATED MODULE: ./package/components/MenusItem.vue?vue&type=template&id=11a672fa

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader-v16/dist??ref--0-1!./package/components/MenusIcon.vue?vue&type=script&lang=js



/* harmony default export */ var MenusIconvue_type_script_lang_js = (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["defineComponent"])({
  name: "Vue3Menus",
  props: {
    options: {
      type: [Function, Object],
      default: {}
    }
  },
  render() {
    if (typeof this.$props.options === 'function') {
      return Object(external_commonjs_vue_commonjs2_vue_root_Vue_["h"])(this.$props.options);
    } else if (typeof this.$props.options.node == 'function' || typeof this.$props.options.node == 'object') {
      return Object(external_commonjs_vue_commonjs2_vue_root_Vue_["h"])(this.$props.options.node, this.$props.options.option);
    } else if (typeof this.$props.options === 'object' && !this.$props.options.node) {
      return Object(external_commonjs_vue_commonjs2_vue_root_Vue_["h"])(this.$props.options);
    }
    return null;
  }
}));

// CONCATENATED MODULE: ./package/components/MenusIcon.vue?vue&type=script&lang=js
 
// CONCATENATED MODULE: ./package/components/MenusIcon.vue



/* harmony default export */ var MenusIcon = (MenusIconvue_type_script_lang_js);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader-v16/dist??ref--0-1!./package/components/MenusItem.vue?vue&type=script&lang=js





/* harmony default export */ var MenusItemvue_type_script_lang_js = (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["defineComponent"])({
  name: "menus-item",
  components: {
    MenusIcon: MenusIcon
  },
  props: {
    menusItemClass: {
      type: String,
      default: null
    },
    hasIcon: {
      type: Boolean,
      default: false
    },
    item: {
      type: Object,
      default: {}
    },
    index: {
      type: Number,
      default: 0
    },
    activeIndex: {
      type: Number,
      default: -1,
    }
  },
  setup(props, { emit, slots }) {
    function menusEnter(event, item) {
      emit("menusEnter", event, item, props.index)
      event.preventDefault();
    }
    function menusClick(event, item) {
      event.preventDefault();
      if (item.disabled) {
        event.stopPropagation();
        return;
      }
      if (
        item &&
        !item.disabled &&
        !item.hidden &&
        typeof item.click === "function"
      ) {
        const val = item.click(item);
        if (val === false || val === null) {
          event.stopPropagation();
        }
      }
    }
    return {
      slots,
      menusEnter,
      menusClick
    };
  },
}));

// CONCATENATED MODULE: ./package/components/MenusItem.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./package/components/MenusItem.vue?vue&type=style&index=0&id=11a672fa&lang=css
var MenusItemvue_type_style_index_0_id_11a672fa_lang_css = __webpack_require__("92c7");

// CONCATENATED MODULE: ./package/components/MenusItem.vue





MenusItemvue_type_script_lang_js.render = MenusItemvue_type_template_id_11a672fa_render

/* harmony default export */ var MenusItem = (MenusItemvue_type_script_lang_js);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader-v16/dist??ref--0-1!./package/components/Menus.vue?vue&type=script&lang=js





/* harmony default export */ var Menusvue_type_script_lang_js = (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["defineComponent"])({
  name: "menus",
  components: {
    MenusItem: MenusItem
  },
  props: {
    menus: {
      type: Array,
      default: []
    },
    menusStyle: {
      type: Object,
      default: {}
    },
    menusItemClass: {
      type: String,
      default: null
    },
    event: {
      type: Object,
      default: {}
    },
    position: {
      type: Object,
      default: {}
    },
    minWidth: {
      type: [Number, String],
      default: 'none'
    },
    maxWidth: {
      type: [Number, String],
      default: 'none'
    },
    zIndex: {
      type: [Number, String],
      default: 3
    },
    direction: {
      type: String,
      default: 'right'
    },
    open: {
      type: Boolean,
      default: false
    }
  },
  setup(props, { slots }) {
    const ctx = {};
    const windowWidth = document.documentElement.clientWidth;
    const windowHeight = document.documentElement.clientHeight;
    const _position = props.position.x && props.position.y ? Object(external_commonjs_vue_commonjs2_vue_root_Vue_["ref"])(props.position) : Object(external_commonjs_vue_commonjs2_vue_root_Vue_["ref"])({
      x: props.event.clientX,
      y: props.event.clientY,
      width: 0,
      height: 0
    });
    const menusRef = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["ref"])(null);
    const style = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["ref"])({
      left: 0,
      top: 0,
      minWidth: `${props.minWidth}px`,
      maxWidth: props.maxWidth == 'none' ? props.maxWidth : `${props.maxWidth}px`,
      zIndex: props.zIndex
    })
    const _direction = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["ref"])(props.direction);
    const activeIndex = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["ref"])(-1);
    const open = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["ref"])(false);
    const hasIcon = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["ref"])(false);

    function leftOpen(menusWidth) {
      style.value.left = _position.value.x - menusWidth;
      _direction.value = 'left';
      if (style.value.left < 0) {
        _direction.value = 'right';
        if (_position.value.width === 0 || _position.value.width === undefined) {
          style.value.left = 0;
        } else {
          style.value.left = _position.valuen.x + _position.value.width;
        }
      }
    }

    function rightOpen(windowWidth, menusWidth) {
      style.value.left = _position.value.x + _position.value.width;
      _direction.value = 'right';
      if (style.value.left + menusWidth > windowWidth) {
        _direction.value = 'left';
        if (_position.value.width === 0 || _position.value.width === undefined) {
          style.value.left = windowWidth - menusWidth;
        } else {
          style.value.left = _position.value.x - menusWidth;
        }
      }
    }

    Object(external_commonjs_vue_commonjs2_vue_root_Vue_["onMounted"])(() => {
      open.value = true;
      props.menus.forEach(menu => {
        hasIcon.value = hasIcon.value || menu.icon !== undefined;
        if (hasIcon.value) {
          return;
        }
      });
      Object(external_commonjs_vue_commonjs2_vue_root_Vue_["nextTick"])(() => {
        const menusWidth = menusRef.value.offsetWidth;
        const menusHeight = menusRef.value.offsetHeight;
        if (_direction.value === 'left') {
          leftOpen(menusWidth);
        } else {
          rightOpen(windowWidth, menusWidth);
        }
        style.value.top = _position.value.y;
        if (_position.value.y + menusHeight > windowHeight) {
          if (_position.value.height === 0 || _position.value.height === undefined) {
            style.value.top = _position.value.y - menusHeight;
          } else {
            style.value.top = windowHeight - menusHeight;
          }
        }
      })
    })

    function menusEnter(event, item, index) {
      activeIndex.value = index;
      if (item.disabled) {
        return;
      }
      if (ctx.instance) {
        if (ctx.index === index) {
          return;
        }
        ctx.instance.close.bind(ctx.instance)();
        ctx.instance = null;
        ctx.index = null;
      }
      if (!item.children) {
        return;
      }
      const menuItemClientRect = event.target.getBoundingClientRect();
      const node = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["h"])(Menus, {
        ...props,
        menus: item.children || [],
        direction: _direction.value,
        position: {
          x: menuItemClientRect.x + 3,
          y: menuItemClientRect.y - 8,
          width: menuItemClientRect.width - 2 * 3,
          height: menuItemClientRect.width
        },
      }, slots);
      const app = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createApp"])(node);
      ctx.instance = app.mount(document.createElement("div"));
      ctx.instance.$unmount = app.unmount;
      ctx.index = index;
      event.preventDefault();
    }
    function close() {
      open.value = false;
      if (this && this.ctx && this.ctx.instance) {
        this.ctx.instance.close();
      }
      Object(external_commonjs_vue_commonjs2_vue_root_Vue_["nextTick"])(() => {
        this.$unmount() && this.$unmount();
      });
    }

    return {
      open,
      hasIcon,
      menusRef,
      style,
      close,
      menusEnter,
      ctx,
      activeIndex,
      slots
    };
  },
}));

// CONCATENATED MODULE: ./package/components/Menus.vue?vue&type=script&lang=js
 
// EXTERNAL MODULE: ./package/components/Menus.vue?vue&type=style&index=0&id=347764fc&scoped=true&lang=css
var Menusvue_type_style_index_0_id_347764fc_scoped_true_lang_css = __webpack_require__("bbcf");

// CONCATENATED MODULE: ./package/components/Menus.vue





Menusvue_type_script_lang_js.render = Menusvue_type_template_id_347764fc_scoped_true_render
Menusvue_type_script_lang_js.__scopeId = "data-v-347764fc"

/* harmony default export */ var Menus = (Menusvue_type_script_lang_js);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader-v16/dist??ref--0-1!./package/components/Vue3Menus.vue?vue&type=script&lang=js




/* harmony default export */ var Vue3Menusvue_type_script_lang_js = (Object(external_commonjs_vue_commonjs2_vue_root_Vue_["defineComponent"])({
  name: "Vue3Menus",
  props: {
    menus: {
      type: Array,
      default: []
    },
    menusStyle: {
      type: Object,
      default: {}
    },
    menusItemClass: {
      type: String,
      default: null
    },
    event: {
      type: Object,
      default: {}
    },
    position: {
      type: Object,
      default: {}
    },
    minWidth: {
      type: [Number, String],
      default: 'none'
    },
    maxWidth: {
      type: [Number, String],
      default: 'none'
    },
    zIndex: {
      type: [Number, String],
      default: 2
    },
    open: {
      type: Boolean,
      default: false
    }
  },
  setup(props, { emit, slots }) {
    let lastInstance = null;

    function mouseEvent() {
      emit("update:open", false);
      if (lastInstance) {
        lastInstance.close();
        lastInstance = null;
      }
    }
    Object(external_commonjs_vue_commonjs2_vue_root_Vue_["watch"])(() => props.open, (newVal) => {
      if (newVal) {
        if (lastInstance) {
          lastInstance.close();
          lastInstance = null;
        }
        const node = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["h"])(Menus, {
          ...props
        }, slots);
        const app = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createApp"])(node);
        lastInstance = app.mount(document.createElement("div"));
        lastInstance.$unmount = app.unmount;
        setTimeout(() => {
          document.addEventListener("click", mouseEvent);
          document.addEventListener("contextmenu", mouseEvent);
          document.addEventListener("wheel", mouseEvent);
        }, 0);
      } else {
        document.removeEventListener("click", mouseEvent);
        document.removeEventListener("contextmenu", mouseEvent);
        document.removeEventListener("wheel", mouseEvent);
      }
    })
    return {}
  },
  render() {
    return null;
  }
}));

// CONCATENATED MODULE: ./package/components/Vue3Menus.vue?vue&type=script&lang=js
 
// CONCATENATED MODULE: ./package/components/Vue3Menus.vue



/* harmony default export */ var Vue3Menus = (Vue3Menusvue_type_script_lang_js);
// CONCATENATED MODULE: ./package/event.js



let event_lastInstance = null;

function event_mouseEvent() {
  if (event_lastInstance) {
    event_lastInstance.close();
    event_lastInstance = null;
  }
  document.removeEventListener("click", event_mouseEvent);
  document.removeEventListener("contextmenu", event_mouseEvent);
  document.removeEventListener("wheel", event_mouseEvent);
}

function $menusEvent(menus, event) {
  const temp = menus || {};
  if (event_lastInstance) {
    event_lastInstance.close();
    event_lastInstance = null;
    document.removeEventListener("click", event_mouseEvent);
    document.removeEventListener("contextmenu", event_mouseEvent);
    document.removeEventListener("wheel", event_mouseEvent);
  }
  let instance = Object(external_commonjs_vue_commonjs2_vue_root_Vue_["createApp"])(Menus, {
    event,
    ...temp
  });
  event_lastInstance = instance.mount(document.createElement("div"));
  event_lastInstance.$unmount = instance.unmount;
  if (temp.prevent == undefined || temp.prevent) {
    event.preventDefault();
  }
  setTimeout(() => {
    document.addEventListener("click", event_mouseEvent);
    document.addEventListener("contextmenu", event_mouseEvent);
    document.addEventListener("wheel", event_mouseEvent);
  }, 0);
  return event_lastInstance;
}

/* harmony default export */ var package_event = ($menusEvent);

// CONCATENATED MODULE: ./package/index.js



Vue3Menus.install = (app, options = {}) => {
  app.component(options.name || Vue3Menus.name, Vue3Menus);
};

const directive = {
  mounted(el, { value, arg, instance }) {
    if (arg === undefined || arg === 'right') {
      el.addEventListener("contextmenu", package_event.bind(instance, value));
    } else if (arg === 'left') {
      el.addEventListener("click", package_event.bind(instance, value));
    } else if (arg === 'all') {
      el.addEventListener("contextmenu", package_event.bind(instance, value));
      el.addEventListener("click", package_event.bind(instance, value));
    }
  },
  unmounted(el, { arg }) {
    if (arg === undefined || arg === 'right') {
      el.removeEventListener("contextmenu", package_event);
    } else if (arg === 'left') {
      el.removeEventListener("click", package_event);
    } else if (arg === 'all') {
      el.removeEventListener("contextmenu", package_event);
      el.removeEventListener("click", package_event);
    }
  }
}

const install = function (app, options = {}) {
  app.component(options.name || Vue3Menus.name, Vue3Menus);
  app.directive('menus', directive);
  app.config.globalProperties.$menusEvent = (event, menus) => package_event(menus, event);
}

if (typeof window !== "undefined" && window.Vue) {
  window.Vue3Menus = install;
}

const menusEvent = (event, menus) => package_event(menus, event);

/* harmony default export */ var package_0 = (install);



// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (package_0);



/***/ })

/******/ });
//# sourceMappingURL=vue3-menus.common.js.map